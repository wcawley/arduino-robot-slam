package com.konartis.arduino.android.robot;

import java.io.IOException;
import java.io.InputStream;

class ReceiveHandler extends Thread {
    private InputStream my_in_data;
    private boolean my_running;

    public ReceiveHandler(final InputStream the_input) {
        my_in_data = the_input;
        my_running = false;
    }

    @Override
    public void run() {
        my_running = true;

        while (my_running) {
            try {
                int length = my_in_data.read();
                length = (my_in_data.read() << 8) + length;
                byte[] returnMessage = new byte[length];
                my_in_data.read(returnMessage);

                synchronized (HomeActivity.my_global_data) {
                    HomeActivity.my_global_data.setNewMessage(returnMessage);
                }

            } catch (final IOException e) {
                // for the bluetooth.read
                e.printStackTrace();
            }
        }
    }

    public void kill() {
        my_running = false;
    }
}