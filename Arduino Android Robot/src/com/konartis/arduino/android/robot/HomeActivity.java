// NEED A STOP BUTTON ON THE MANUAL CONTROL SCREEN

package com.konartis.arduino.android.robot;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends Activity {

    public static PublicData my_global_data;

    private static final int CREATE_CONNECTION = 0;
    private static final int MAIN_MENU = 1;
    private static final int OTHER_SCREEN = 2;
    private int my_current_screen;

    private BluetoothSocket my_bt_socket;
    private OutputStream my_out_data;
    private InputStream my_in_data;
    private ReceiveHandler my_receive_thread;
    private MoveThread my_wander_thread;
    private Navigator my_navigation_thread;
    private MessageViewThread my_message_thread;

    @Override
    public void onCreate(final Bundle the_saved_instance_state) {
        super.onCreate(the_saved_instance_state);
        my_current_screen = CREATE_CONNECTION;
        setContentView(R.layout.layout_create_connection);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu the_menu) {
        getMenuInflater().inflate(R.menu.activity_home, the_menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (my_current_screen == CREATE_CONNECTION) {
            finish();
        } else if (my_current_screen == MAIN_MENU) {
            disconnectBT();
            my_current_screen = CREATE_CONNECTION;
            setContentView(R.layout.layout_create_connection);
        } else {
            if (my_message_thread.isAlive()) {
                my_message_thread.kill();
            }
            my_current_screen = MAIN_MENU;
            setContentView(R.layout.layout_main_menu);
        }
    }

    // THIS COULD BE MOVED INTO THE BLUETOOTH MANAGER

    public void buttonConnectHandler(final View the_view) {
        // get an object to interact with the bluetooth adapter, if the device
        // has one
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            // Device does not support Bluetooth
        }

        // if the bluetooth device is not enabled then ask to enable
        if (!btAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }
        
        BluetoothDevice btDevice = null;
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        if(pairedDevices.size() > 0) {
            for(BluetoothDevice device : pairedDevices) {
                if(device.getName().equals("HC-07")) {
                    btDevice = device;
                    break;
                }
            }
        }
        
        btAdapter.cancelDiscovery();

        // this blocks until connection is made
        try {
            my_bt_socket = btDevice.createRfcommSocketToServiceRecord(UUID
                    .fromString("00001101-0000-1000-8000-00805F9B34FB"));
            my_bt_socket.connect();
            my_out_data = my_bt_socket.getOutputStream();
            my_in_data = my_bt_socket.getInputStream();

        } catch (final IOException ioe) {
            Log.i("Create Connection", "Failed to create connection or get stream.");
        }

        my_global_data = new PublicData();

        // create the message receive thread
        my_receive_thread = new ReceiveHandler(my_in_data);
        my_wander_thread = new MoveThread(my_out_data);
        my_navigation_thread = new Navigator();
        my_message_thread = new MessageViewThread();
        my_receive_thread.start();

        my_current_screen = MAIN_MENU;
        setContentView(R.layout.layout_main_menu);
    }

    public void buttonExploreHandler(final View the_view) {
        my_current_screen = OTHER_SCREEN;
        setContentView(R.layout.layout_exploration);
    }

    public void buttonMapHandler(final View the_view) {
        my_current_screen = OTHER_SCREEN;
        setContentView(R.layout.layout_map);
    }

    public void buttonMessagesHandler(final View the_view) {
        // START THREAD HERE TO KEEP MESSAGE VIEW UP TO DATE
        my_current_screen = OTHER_SCREEN;
        setContentView(R.layout.layout_message_view);
    }

    public void buttonManualControlHandler(final View the_view) {
        my_current_screen = OTHER_SCREEN;
        setContentView(R.layout.layout_manual_control);
    }

    public void buttonStartPauseHandler(final View the_view) {
        Button b = (Button) the_view.findViewById(R.id.buttonStartPause);

        if (my_wander_thread.isAlive()) {
            b.setText("Start Exploring");
            my_wander_thread.kill();
            my_navigation_thread.kill();
        } else {
            b.setText("Stop Exploring");
            my_global_data.setNextAction(new byte[] { 0, 0, 0, 125 });
            my_wander_thread.start();
            my_navigation_thread.start();
        }
    }

    public void buttonMainMenuHandler(final View the_view) {
        setContentView(R.layout.layout_main_menu);
    }

    public void buttonUpHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(1).array());
    }

    public void buttonDownHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(2).array());
    }

    public void buttonRightHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(3).array());
    }

    public void buttonLeftHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(4).array());
    }

    public void buttonRotateRightHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(6).array());
    }

    public void buttonRotateLeftHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(7).array());
    }

    public void buttonPingUSHandler(final View the_view) {
        sendMessage(ByteBuffer.allocate(4).putInt(15).array());
    }

    public void buttonDisconnectHandler(final View the_view) {
        disconnectBT();
        my_current_screen = CREATE_CONNECTION;
        setContentView(R.layout.layout_create_connection);
    }

    public void buttonExitHandler(final View the_view) {
        finish();
    }
    
    public void b1(final View the_view) {
    	sendMessage(ByteBuffer.allocate(4).putInt(0).array());
    }
    
    public void b2(final View the_view) {
    	sendMessage(ByteBuffer.allocate(4).putInt(1).array());
    }

    // THESE TWO METHODS CAN BE MOVED INTO BLUETOOTH MANAGER

    private void disconnectBT() {
        my_receive_thread.kill();
        my_wander_thread.kill();
        my_navigation_thread.kill();
        my_message_thread.kill();

        try {
            sendMessage(ByteBuffer.allocate(4).putInt(0).array());
            my_in_data.close();
            my_out_data.close();
            my_bt_socket.close();
        } catch (final IOException ioe) {
            Log.i("Disconnect", "Error Disconnecting or closing stream or socket.");
        }
    }

    // sendMessage(ByteBuffer.allocate(4).putInt(VALUE).array());
    // max of 127
    private void sendMessage(final byte[] the_message) {
        try {
            int messageLength = the_message.length;
            my_out_data.write(messageLength);
            my_out_data.write(messageLength >> 8);
            my_out_data.write(the_message, 0, the_message.length);
        } catch (final IOException ioe) {
            Log.i("sendMessage", "Error sending message to NXT.");
        }
    }
}