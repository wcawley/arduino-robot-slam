package com.konartis.arduino.android.robot;

import android.widget.TextView;

public class MessageViewThread extends Thread {

    private int my_number_of_messages;
    private TextView my_message_view;
    private TextView my_x_view;
    private TextView my_y_view;
    private TextView my_dir_view;
    private boolean my_running;

    public MessageViewThread() {
        my_number_of_messages = 0;
        my_message_view = null;
        my_x_view = null;
        my_y_view = null;
        my_dir_view = null;
        my_running = false;
    }

    @Override
    public void run() {
        my_running = true;

        // find and assign all the views
        while (my_running) {

        }

    }

    public void kill() {
        my_running = false;
    }
}
