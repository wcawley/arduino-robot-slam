package com.konartis.arduino.android.robot;

import java.io.IOException;
import java.io.OutputStream;

import android.util.Log;

class MoveThread extends Thread {
    private OutputStream my_out_data;
    private boolean my_running;

    public MoveThread(final OutputStream the_output) {
        my_out_data = the_output;
        my_running = false;
    }

    @Override
    public void run() {

        my_running = true;

        while (my_running) {

            // wait until the main program has found the next action to make
            boolean newAction = false;
            while (!newAction) {
                synchronized (HomeActivity.my_global_data) {
                    if (HomeActivity.my_global_data.getNextAction() != null) {
                        newAction = true;
                    }
                }

                if (!newAction) {
                    try {
                        sleep(100);
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            // get the next motor command and reset the data
            byte[] message = null;
            synchronized (HomeActivity.my_global_data) {
                message = HomeActivity.my_global_data.getNextAction();
                HomeActivity.my_global_data.setNextAction(null);
                // update current position to where it will be when this action
                // completes
                calculateNewPosition(message);
            }

            sendMessage(message);
        }
    }

    // value is multiplied by 3 on the nxt side
    // so 120 = 1 full rotation or 26cm
    // 26 / 120 == distance per command value

    // one stationary and other 360 will 90 degree turn
    // wheel base 16.5cm
    // movement is currently always straight or a turn in place

    // 20, -20 == turn right 30 degree (20 * 3) = 60 / 2 = 30
    private void calculateNewPosition(final byte[] the_command) {
        double newX = HomeActivity.my_global_data.getX();
        double newY = HomeActivity.my_global_data.getY();
        double newDir = HomeActivity.my_global_data.getDirection();

        // going straight
        if (the_command[0] == the_command[1]) {
            double distTraveled = ((double) 26 / 120) * the_command[0];
            newX += distTraveled * Math.cos(newDir);
            newY += distTraveled * Math.sin(newDir);

            // turning
        } else {
            newDir += (the_command[0] * 3) / 2;
        }

        // THIS ROUNDING TO INT IS GOING TO CAUSE ERROR OVER A DISTANCE
        // MAYBE MAKE THE VALUES IN GLOBAL DATA DOUBLES
        HomeActivity.my_global_data.setX((int) newX);
        HomeActivity.my_global_data.setY((int) newY);
        HomeActivity.my_global_data.setDirection((int) newDir);
    }

    private void sendMessage(final byte[] the_message) {
        try {
            int messageLength = the_message.length;
            my_out_data.write(messageLength);
            my_out_data.write(messageLength >> 8);
            my_out_data.write(the_message, 0, the_message.length);
        } catch (final IOException ioe) {
            Log.i("sendMessage", "Error sending message to NXT.");
        }
    }

    public void kill() {
        my_running = false;
    }
}