package com.konartis.arduino.android.robot;

import java.util.ArrayList;

import android.graphics.Point;

public class Cartographer {

	private Thread my_running;
	
	public Cartographer() {
		my_running = new Thread();
		// setup the thread to check for data in the unprocessed sensor readings
	}
	
	private void updateMapData() {
		ArrayList<Reading> newReadings = myData.getNewReadings();
		
		for (Reading r : newReadings) {
			double angle = 0;
			// calc center reading
			angle = r.getDir();
			addMapPoint(r.getCenter(), angle);

			// calc right reading
			angle = (r.getDir() + 45) % 360;
			addMapPoint(r.getRight(), angle);

			// calc left reading
			angle = r.getDir() - 45;
			if (angle < 0) {
				angle += 360;
			}
			addMapPoint(r.getLeft(), angle);
		}
	}

	private void addMapPoint(final double the_dist, final double the_angle) {
		double x = 0;
		double y = 0;
		double effectiveAngle = 0;

		if (the_angle < 90) {
			effectiveAngle = the_angle;
			x = Math.cos(effectiveAngle) * the_dist;
			y = Math.sin(effectiveAngle) * the_dist;

		} else if (the_angle < 180) {
			effectiveAngle = Math.abs(the_angle - 180);
			x = Math.cos(effectiveAngle) * the_dist;
			y = Math.sin(effectiveAngle) * the_dist;
			y = y * -1;

		} else if (the_angle < 270) {
			effectiveAngle = the_angle - 180;
			x = Math.cos(effectiveAngle) * the_dist;
			y = Math.sin(effectiveAngle) * the_dist;
			x = x * -1;
			y = y * -1;

		} else {
			effectiveAngle = Math.abs(the_angle - 360);
			x = Math.cos(effectiveAngle) * the_dist;
			y = Math.sin(effectiveAngle) * the_dist;
			x = x * -1;
		}

		myData.addMapData(x, y);
	}
}
