package com.konartis.arduino.android.robot;

public class Reading {
	private double my_x;
	private double my_y;
	private double my_dir;
	private double my_left;
	private double my_center;
	private double my_right;

	public Reading(final double the_x, final double the_y,
			final double the_dir, final double the_right,
			final double the_center, final double the_left) {
		my_x = the_x;
		my_y = the_y;
		my_dir = the_dir;
		my_left = the_left;
		my_center = the_center;
		my_right = the_right;
	}

	public double getX() {
		return my_x;
	}

	public double getY() {
		return my_y;
	}

	public double getDir() {
		return my_dir;
	}

	public double getLeft() {
		return my_left;
	}

	public double getCenter() {
		return my_center;
	}

	public double getRight() {
		return my_right;
	}
}
