package com.konartis.arduino.android.robot;

import java.util.ArrayList;

// THIS CLASS NEEDS TO BE THREAD SAFE... AS MULTIPLE
// THREADS WILL BE TRYING TO ACCESS THIS DATA

public class PublicData {

	// position contains the position after the action completes
	private int my_x;
	private int my_y;
	private int my_direction;
	private byte[] my_next_action;
	private int[][] my_map;
	private byte[] my_new_message;
	private ArrayList<byte[]> my_messages;

	public PublicData() {
		my_x = 0;
		my_y = 0;
		my_direction = 0;
		my_next_action = null;
		my_map = new int[200][200];
		my_new_message = null;
		my_messages = new ArrayList<byte[]>();
	}

	public void setX(final int the_x) {
		my_x = the_x;
	}

	public void setY(final int the_y) {
		my_y = the_y;
	}

	public void setDirection(final int the_direction) {
		my_direction = the_direction;
	}

	public void setNextAction(final byte[] the_next_action) {
		my_next_action = the_next_action;
	}

	public void setMapData(final int the_x, final int the_y, final int the_data) {
		my_map[the_x][the_y] = the_data;
	}

	public void setNewMessage(final byte[] the_message) {
		my_new_message = the_message;
		my_messages.add(the_message);
	}

	public int getX() {
		return my_x;
	}

	public int getY() {
		return my_y;
	}

	public int getDirection() {
		return my_direction;
	}

	public byte[] getNextAction() {
		return my_next_action;
	}

	public int[][] getMap() {
		return my_map;
	}

	public byte[] getNewMessage() {
		return my_new_message;
	}
}
