package com.konartis.arduino.android.robot;

import java.util.Random;

public class Navigator extends Thread {
    private static final int EXPLORATION = 0;
    private static final int MANUAL = 1;

    private int my_current_state;
    private boolean my_running;

    public Navigator() {
        my_current_state = MANUAL;
        my_running = false;
    }

    @Override
    public void run() {

        my_running = true;

        while (my_running) {

            // wait until there is a new message to process
            boolean newMessage = false;
            while (!newMessage) {
                synchronized (HomeActivity.my_global_data) {
                    if (HomeActivity.my_global_data.getNewMessage() != null) {
                        newMessage = true;
                    }
                }

                if (!newMessage) {
                    try {
                        sleep(100);
                    } catch (final InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            // get the message data from previous action
            synchronized (HomeActivity.my_global_data) {
                byte[] sensorData = HomeActivity.my_global_data.getNewMessage();
                HomeActivity.my_global_data.setNewMessage(null);

                byte[] message = null;

                // process the sensor data and create next command
                // COULD TRY FUSION BETWEEN EACH READING TO DETERMINE DIRECTION
                int usLeftReading = (sensorData[1] * 128) + sensorData[2];
                int usFrontReading = (sensorData[3] * 128) + sensorData[4];
                int usRightReading = (sensorData[5] * 128) + sensorData[6];

                if (usFrontReading < 40) {
                    // avoid the object in front
                    if (usRightReading > 40 && usLeftReading > 40) {
                        // turn left
                        // THIS SHOULD BE A RANDOM BETWEEN LEFT OR RIGHT
                        // OR CHOOSE THE ONE WITH MORE SPACE
                        message = new byte[] { -20, 20, 0, 0 };

                    } else if (usRightReading < 40) {
                        // turn left
                        message = new byte[] { -20, 20, 0, 0 };

                    } else if (usLeftReading < 40) {
                        // turn right
                        message = new byte[] { 20, -20, 0, 0 };
                    }
                } else {
                    message = moveRandom();
                }

                // update the location data

                // set the next movement command
                HomeActivity.my_global_data.setNextAction(message);
            }
        }
    }

    // left motor... right motor
    // measured in degree's (will be multiplied by 3)
    private byte[] moveRandom() {
        final Random rand = new Random();
        byte[] message = null;
        int direction = rand.nextInt(5);

        if (direction == 0) {
            byte turn = (byte) (20 + rand.nextInt(35));
            message = new byte[] { turn, (byte) -turn, 0, 0 };

        } else if (direction == 1) {
            byte turn = (byte) (20 + rand.nextInt(35));
            message = new byte[] { (byte) -turn, turn, 0, 0 };

        } else {
            message = new byte[] { 80, 80, 0, 0 };
        }

        return message;
    }

    public void moveToGoal(final int the_x, final int the_y) {

    }

    public void kill() {
        my_running = false;
    }
}
